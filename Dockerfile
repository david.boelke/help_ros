# start form ROS-melodic
FROM osrf/ros:melodic-desktop-full

# install important programs
RUN apt update; apt install -y vim tmux git sudo stow

# add nonroot user 'user' with sudo abilities and nice configurations
RUN adduser user --home /home/user
RUN echo 'root:root'|chpasswd
RUN echo 'user:user'|chpasswd
RUN echo "user ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers
#RUN echo "source /opt/ros/melodic/setup.bash" >> /home/user/.bashrc
# covenience, this entire block can be removed without affecting the functionality
USER user
WORKDIR /home/user
RUN git clone https://www.github.com/bananabook/dotfiles.git -b mini
WORKDIR /home/user/dotfiles
RUN stow vim tmux git
RUN echo "source ~/dotfiles/bash/.bashrc" >> /home/user/.bashrc
RUN echo 'PS1="\u:\wΣ "' >> /home/user/.bashrc

# setup workspace
RUN mkdir -p  /home/user/catkin_ws/src
WORKDIR /home/user/catkin_ws/src
#COPY manipulability_metrics /home/user/catkin_ws/src
#RUN git clone https://github.com/tecnalia-medical-robotics/manipulability_metrics

# entry script
#COPY scripts/* /home/user/catkin_ws/scripts/
WORKDIR /home/user/catkin_ws
USER root
#RUN chmod u+x /home/user/catkin_ws/scripts/* && chown user: /home/user/catkin_ws/scripts/*
COPY ./scripts/all.sh /home/user/entrypoint.sh
RUN chmod +x /home/user/entrypoint.sh
USER user

CMD ["/bin/bash", "/home/user/entrypoint.sh"]
