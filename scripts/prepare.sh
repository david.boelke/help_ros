# install dependencies
rosdep update
rosdep install --from-paths src --ignore-src -r -y

# make and setup
catkin_make
source devel/setup.bash
