## prepare
# install dependencies
rosdep update
rosdep install --from-paths src --ignore-src -r -y

# make and setup
catkin_make
source devel/setup.bash

## start
# run launch file from example
roslaunch manipulability_metrics_examples panda_manipulability_demo.launch
