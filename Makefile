all: build run
build:
	echo buidling:|figlet
	docker build -t container .
run:
	echo running:|figlet
	docker run \
	--rm \
	-e DISPLAY=${DISPLAY} \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
		-v $(shell readlink -e ./manipulability_metrics):/home/user/catkin_ws/src \
		-v $(shell readlink -e ./scripts):/home/user/catkin_ws/scripts \
		-it container 
