# How to use
Just call `make`. Depending on your installation of docker, you might need elevated permissions to build and run the dockercontainer.

# What does not work
The roslaunch command returns an error message, that I cannot get wise out of. This is the message:
```sh
RLException: [panda_manipulability_demo.launch] is neither a launch file in package [manipulability_metrics_examples] nor is [manipulability_metrics_examples] a launch file name
The traceback for the exception was written to the log file
```
